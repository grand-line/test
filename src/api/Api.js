import { http } from '../utils/http'

class Api {
  getUsers(login) {
    return new Promise((resolve, reject) => {
      http.get(`search/users?q=${login}`)
        .then(({ data }) => {
          resolve(data);
        })
        .catch(error => reject(error));
    })
  }
}

export default new Api()
