import React from 'react';
import ReactPaginate from 'react-paginate';

const Pagination = ({
  pageCount,
  onPageChange,
  initialPage
}) => {

  return (
    <>
      <ReactPaginate
        previousLabel={'previous'}
        nextLabel={'next'}
        breakLabel={'...'}
        pageCount={pageCount}
        initialPage={initialPage}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={onPageChange}
        breakClassName={'page-item'}
        breakLinkClassName={'page-link'}
        containerClassName={'pagination justify-content-center'}
        pageClassName={'page-item'}
        pageLinkClassName={'page-link'}
        previousClassName={'page-item'}
        previousLinkClassName={'page-link'}
        nextClassName={'page-item'}
        nextLinkClassName={'page-link'}
        activeClassName={'active'}
      />
    </>
  )
}

export default Pagination;
