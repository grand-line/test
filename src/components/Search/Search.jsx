import React, { useState } from 'react'
import { Button, Input, InputGroup, InputGroupAddon } from 'reactstrap'

const Search = ({ value, onChange, onClick }) => {

  return (
    <InputGroup>
      <Input
        value={value}
        onChange={({ target }) => onChange(target.value)}
      />
      <InputGroupAddon addonType="prepend">
        <Button
          disabled={!value?.length}
          onClick={onClick}
        >
          Search
        </Button>
      </InputGroupAddon>
    </InputGroup>
  )
}

export default Search
