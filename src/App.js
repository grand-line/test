import React, { useEffect, useState } from 'react'
import Api from './api/Api'
import { Pagination, Search } from './components'
import { Col, Container, ListGroup, Row, Spinner } from 'reactstrap'

const ViewList = ({ listData }) => (
  <>
    {listData?.map(({ login, avatar_url, type }, index) => (
      <Row className={'mb-5'}>
        <Col xs={3}>
          <div className={'users-list__image'}>
            <img className={'h-100'} src={avatar_url}/>
          </div>
        </Col>
        <Col xs={3} className={'d-flex align-items-center'}>
          <div>
            {login}
          </div>
        </Col>
        <Col xs={3} className={'d-flex align-items-center'}>
          {type}
        </Col>
      </Row>
    ))
    }
  </>
)

const App = () => {

  const [totalPages, setTotalPages] = useState(0)
  const [isLoading, setIsLoading] = useState(false)
  const [searchValue, setSearchValue ] = useState('')
  const [ paginationPage, setPaginationPage ] = useState(1)
  const [ userData, setUserData ] = useState(null)

  const attemptGetDataUsers = async (login) => {
    setIsLoading(true)
    try {
      const { items } = await Api.getUsers(login)
      const pages = items?.length && (Object.keys(items).length / 9).toFixed(0)
      setIsLoading(false)
      set
    } catch (e) {
      // console.log(e)
    }
  }

  return (
    <div className={'d-flex flex-column'}>
      <div className={'bg-secondary container-fluid mb-5'}>
        <Container>
          <p className={'font-weight-bold mt-2 display-1 text-white'}>Get users</p>
        </Container>
      </div>
      <div>
        <Container>
          <div className={'w-50 mb-5'}>
            <Search
              onChange={setSearchValue}
              value={searchValue}
              onClick={() => attemptGetDataUsers(searchValue)}
            />
          </div>

          <div className={'mb-5 users-list'}>
            {isLoading
              ? <div className={'h-75 d-flex justify-content-center align-items-center'}>
                <Spinner color="secondary"/>
              </div>
              : <div>
                <ViewList listData={userData}/>
              </div>
            }
          </div>
          <div>
            <Pagination
              initialPage={paginationPage}
              onPageChange={({ selected: selectedPage }) => setPaginationPage(selectedPage)}
              pageCount={totalPages}
            />
          </div>

        </Container>
      </div>

    </div>
  )
}

export default App

