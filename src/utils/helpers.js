export const errorsTypes = {
  404: 'Not Found',
  401: 'Unauthorized',
  422: 'Unprocessable Entity',
  406: 'Not Acceptable',
  500: 'Server error',
  400: 'Request error'
};
