import axios from 'axios';

const config = {
  http: {
    defaultRequest: {
      headers: {
        'Accept': 'application/vnd.github.v3+json',
      }
    },
    baseURL: 'https://api.github.com/'
  },
}

const http = axios.create({
  baseURL: `${config.http.baseURL}`,
  headers: config.http.defaultRequest.headers,
})

const beforeResponseSuccess = response => response
const beforeResponseError = error => error

http.interceptors.response.use(beforeResponseSuccess, beforeResponseError)

export {
  http
}

